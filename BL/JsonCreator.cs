﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace PortabelloRoni.BL
{
    public class JsonCreator
    {

       
        [JsonProperty("RecordingMode")]
        public bool? RecordingMode { get; set; }
        [JsonProperty("Detector")]
        public bool? Detector { get; set; }
        [JsonProperty("RecordingTime")]
        public double RecordingTime { get; set; }
        [JsonProperty("FPS")]
        public double FPS { get; set; }
        [JsonProperty("WifiName")]
        public string WifiName { get; set; }
        [JsonProperty("Password")]
        public string Password { get; set; }
        [JsonProperty("ParseScan")]
        public double ParseScan { get; set; }


        public JsonCreator() { }

        public JsonCreator(JsonCreator j)
        {
            this.RecordingMode = j.RecordingMode;
            this.Detector = j.Detector;
            this.RecordingTime = j.RecordingTime;
            this.FPS = j.FPS;
            this.WifiName = j.WifiName;
            this.Password = j.Password;
            this.ParseScan = j.ParseScan;

        }


        public JsonCreator(bool? RecordingMode, bool? Detector, double RecordingTime, double FPS, string WifiName, string Password, double ParseScan)
        {
           
            this.RecordingMode = RecordingMode;
            this.Detector = Detector;
            this.RecordingTime = RecordingTime;
            this.FPS = FPS;
            this.WifiName = WifiName;
            this.Password = Password;
            this.ParseScan = ParseScan;
        }



        public string JsonCreatorReturn()
        {

            JsonCreator Newjson = new JsonCreator(Logic.Instance._RecordingMode, Logic.Instance._Detector, Logic.Instance._RecordingTime, Logic.Instance._FPS, Logic.Instance._WifiName, Logic.Instance._Password, Logic.Instance._ParseScan);

            JObject obj = JObject.Parse(JsonConvert.SerializeObject(Newjson));

            if (Logic.Instance._RecordingMode != Logic.Instance.JsonInstance.RecordingMode)
            {
                Logic.Instance.JsonInstance.RecordingMode = Logic.Instance._RecordingMode;
            }
            else
            {
                obj.Remove("RecordingMode");
            }

            if (Logic.Instance._RecordingTime != Logic.Instance.JsonInstance.RecordingTime)
            {
                Logic.Instance.JsonInstance.RecordingTime = Logic.Instance._RecordingTime;
            }
            else
            {
                obj.Remove("RecordingTime");
            }

            if (Logic.Instance._Detector != Logic.Instance.JsonInstance.Detector)
            {
                Logic.Instance.JsonInstance.Detector = Logic.Instance._Detector;
            }
            else
            {
                obj.Remove("Detector");
            }



            if (Logic.Instance._FPS != Logic.Instance.JsonInstance.FPS)
            {

                Logic.Instance.JsonInstance.FPS =Logic.Instance._FPS;

            }
            else
            {
                obj.Remove("FPS");
            }

            if (Logic.Instance._WifiName != Logic.Instance.JsonInstance.WifiName)
            {
                Logic.Instance.JsonInstance.WifiName = Logic.Instance._WifiName;

            }
            else
            {
                obj.Remove("WifiName");
            }

            if (Logic.Instance._Password != Logic.Instance.JsonInstance.Password)
            {
                Logic.Instance.JsonInstance.Password = Logic.Instance._Password;
            }
            else
            {
                obj.Remove("Password");
            }

            if (Logic.Instance._ParseScan != Logic.Instance.JsonInstance.ParseScan)
            {
                Logic.Instance.JsonInstance.ParseScan = Logic.Instance._ParseScan;

            }
            else
            {
                obj.Remove("ParseScan");
            }

            return obj.ToString();
        }

   

    }
}

