﻿using PortabelloRoni.BL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FileTransferProtocolLib;

namespace PortabelloRoni
{
    public partial class FTPFilesClient : Form
    {

        FTPManagerClass client;
        FTP manager;

        public FTPFilesClient()
        {
            InitializeComponent();
        }
        private void button8_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            string[] files = manager.FilesOnServer(Server.);
            foreach (string filename in files)
            {
                dataGridView1.Rows.Add(filename);
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            client = new FTPManagerClass(textBox1.Text, textBox2.Text, textBox3.Text);
            manager = new FTP(textBox3.Text, textBox1.Text, textBox2.Text);
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (Convert.ToString(dataGridView1.Rows[e.RowIndex].Cells[0].Value).Contains("."))
            {
                return;
            }
            else
            {
                string[] files = manager.FilesOnServer(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
                dataGridView1.Rows.Clear();
                foreach (string file in files)
                {
                    dataGridView1.Rows.Add(file);
                }
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void button6_Click(object sender, EventArgs e)
        {
            manager.CreateDirectory(textBox4.Text);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            manager.Delete(textBox11.Text);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            manager.ReName(textBox10.Text, textBox9.Text);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "All files|*.*";
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textBox5.Text = ofd.FileName;
                textBox6.Text = ofd.SafeFileName;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
           manager.DownloadFile(textBox7.Text, textBox8.Text);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            manager.UploadFile(textBox5.Text, textBox6.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            string ext = textBox7.Text.Substring(textBox7.Text.IndexOf("."));
            sfd.Filter = "Download file extension(" + ext + ")|*" + ext;
            if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textBox8.Text = sfd.FileName;
            }
        }

      
    }
}
