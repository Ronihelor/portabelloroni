﻿using System;
using System.Windows;
using System.Windows.Media.Imaging;
using Newtonsoft.Json;
using System.Windows.Threading;
using PortabelloRoni.BL;
using PortabelloRoni.Global;


namespace PortabelloRoni
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    { 

        public MainWindow()
        {
            InitializeComponent();

            BitmapImage bImage = new BitmapImage(new Uri(@"C:\Users\Administrator\Documents\Visual Studio 2015\Projects\PortabelloRoni\PortabelloRoni\Assets\Images\stream-unavailable.png"));
            Logic.Instance.CamMan.onTriggerFunction = Client_DataRecieved;
            Logic.Instance.TCPIPMan.onTriggerFunction = Client_DataRecieved;
            Logic.Instance._ImageSource = bImage;
            this.DataContext = Logic.Instance;
            Logic.Instance.TCPIPMan.Load();
            Logic.Instance.CamMan.Load();

            Logic.Instance._Ip = StaticNotification.IP;
            Logic.Instance._Port = StaticNotification.Port;
            Logic.Instance._PortCamera = StaticNotification.PortCamera;
            Logic.Instance._Width = StaticNotification.WidthVid;
            Logic.Instance._Height = StaticNotification.HeightVid;

   
        

        }



        public void Client_DataRecieved(string e, bool IsCamera)
        {

         
                string replyMessage = e.Replace("\u0013", "");

                if (replyMessage == StaticNotification.Connected || replyMessage == StaticNotification.FailedConnection)
                {        
                        Logic.Instance._ErrorText = (replyMessage);   
                }
                else
                {
                    try
                    {
                        string json = replyMessage;
                    if (IsCamera == true)
                    {
                        Dispatcher.BeginInvoke(new Action(delegate
                        {
                            Logic.Instance._ImageSource = Logic.Instance.ToImage(Convert.FromBase64String(json));
                        }));
                    }

                    else
                    {
                        JsonCreator newJson = JsonConvert.DeserializeObject<JsonCreator>(json);
                        Logic.Instance.JsonInstance = new JsonCreator(newJson);
                        Logic.Instance.loadtointerface(newJson);
                    }

                    }
                    catch 
                    {

                            Logic.Instance._ErrorText = (StaticNotification.Error);         
                }
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            FTPFilesClient windowforn = new FTPFilesClient();
            windowforn.Show();
        }

     
    }
}
