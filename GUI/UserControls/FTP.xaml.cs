﻿using System.Windows.Controls;
using PortabelloRoni.DAL;
using PortabelloRoni.Global;
using PortabelloRoni.BL;



namespace PortabelloRoni.GUI.UserControls
{
    /// <summary>
    /// Interaction logic for FTP.xaml
    /// </summary>
    public partial class FTP : UserControl
    {
        FTPManagerClass Client;

        public FTP()
        {
            InitializeComponent();
        }

        private void buttonLogin_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Client = new FTPManagerClass(UserText.Text, PasswordText.Text, StaticNotification.IP);
        }

        private void buttonGetFile_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            string[] files = Client.getFilesOnServer("");



        }

     
    }
}
